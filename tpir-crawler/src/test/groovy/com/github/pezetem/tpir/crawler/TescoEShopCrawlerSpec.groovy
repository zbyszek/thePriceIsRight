package com.github.pezetem.tpir.crawler

import org.jsoup.Jsoup
import org.springframework.core.io.ClassPathResource
import spock.lang.Specification
import spock.lang.Subject

class TescoEShopCrawlerSpec extends Specification {

    @Subject
    TescoEShopCrawler tescoCrawler = new TescoEShopCrawler();

    def "should verify if all tesco ulrs have at least one sub page"() {
        expect:
            tescoCrawler.findNoPaginations(Jsoup.connect(tescoUrls).get()) > 1
        where:
            tescoUrls << new ClassPathResource("pages/tesco/tescoUrls.txt").getFile().readLines()
    }
}
