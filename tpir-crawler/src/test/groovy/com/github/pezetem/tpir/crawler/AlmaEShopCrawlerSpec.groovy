package com.github.pezetem.tpir.crawler

import com.github.pezetem.tpir.crawler.cookies.CookieRestTemplate
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpEntity
import spock.lang.Specification
import spock.lang.Subject

class AlmaEShopCrawlerSpec extends Specification {

    String SESSION_INIT_URL = "http://www.alma24.pl/web/session/init?store=default";
    @Subject
    CookieRestTemplate template = new CookieRestTemplate().withSessionInitUrl(SESSION_INIT_URL);

    def "should verify if all alma pages are valid"(){
        expect:
            HttpEntity<String> response = template.getForEntity(almaUrls, String.class);
            response.getStatusCode().value() == 200
        where:
            almaUrls << new ClassPathResource("pages/alma/almaUrls.txt").getFile().readLines()
    }
}
