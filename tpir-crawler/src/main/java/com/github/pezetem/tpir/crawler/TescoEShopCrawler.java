package com.github.pezetem.tpir.crawler;

import com.google.common.annotations.VisibleForTesting;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Component
public class TescoEShopCrawler implements EShopCrawler {
    private String EMTPY_URL = "";
    private String PAGE_NO = "&pageNo=";
    private String SORT_BY_DEFAULT = "&sortBy=Default";

    @Override
    public List<String> downloadHtmlPageWithSubpages(String baseUrl) {
        Optional<Document> basePageDocument = getPageDocument(baseUrl);

        Document document = basePageDocument.orElse(new Document(baseUrl));
        int noPages = findNoPaginations(document);

        return downloadAllSubPages(baseUrl, noPages);
    }

    private List<String> downloadAllSubPages(String pageUrl, int noPages) {
        log.info("Getting all {} subpages from url {}", noPages, pageUrl);
        return IntStream.rangeClosed(1, noPages)
                .boxed()
                .parallel()
                .map(i -> getPageDocument(pageUrl + PAGE_NO + i + SORT_BY_DEFAULT)
                        .orElse(new Document(EMTPY_URL))
                        .toString())
                .collect(Collectors.toList());
    }

    @VisibleForTesting
    int findNoPaginations(Document document) {
        Element paginationBar = getFirstPaginationBar(document);
        Elements lastPage = selectLastPageElement(paginationBar);
        return Integer.parseInt(extractMaxPageNumber(lastPage));
    }

    private Elements selectLastPageElement(Element paginationBar) {
        return paginationBar.select("li:last-child")
                    .stream()
                    .findFirst()
                    .map(e -> e.select("a")).get();
    }

    private String extractMaxPageNumber(Elements lastPage) {
        return lastPage.get(0).childNode(1).toString();
    }

    private Element getFirstPaginationBar(Document document) {
        return document.select(".paginatedNavigation .pagination").get(0);
    }

    private Optional<Document> getPageDocument(String pageUrl) {
        try {
            return Optional.of(Jsoup.connect(pageUrl).get());
        } catch (IOException e) {
            log.info("Error while downloading page {}, message: {}", pageUrl, e.getMessage());
        }
        return Optional.empty();
    }
}
