package com.github.pezetem.tpir.crawler;

import java.util.List;

public interface EShopCrawler {
    List<String> downloadHtmlPageWithSubpages(String pageUrl);
}
