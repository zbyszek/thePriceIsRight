package com.github.pezetem.tpir.crawler;

import com.github.pezetem.tpir.crawler.cookies.CookieRestTemplate;
import org.springframework.http.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class AlmaEShopCrawler implements EShopCrawler {
    private final String SESSION_INIT_URL = "http://www.alma24.pl/web/session/init?store=default";

    @Override
    public List<String> downloadHtmlPageWithSubpages(String pageUrl) {
        CookieRestTemplate template = new CookieRestTemplate()
                .withSessionInitUrl(SESSION_INIT_URL);
        HttpEntity<String> response = template.getForEntity(pageUrl, String.class);
        return Arrays.asList(response.getBody());
    }
}
