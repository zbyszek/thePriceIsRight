package com.github.pezetem.tpir.crawler.cookies;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.util.List;

public class CookieRestTemplate extends RestTemplate {
    private String sessionInitUrl;

    @Override
    protected ClientHttpRequest createRequest(URI url, HttpMethod method) throws IOException {
        ClientHttpRequest request = super.createRequest(url, method);

        String cookie = getSessionCookie(sessionInitUrl);
        request.getHeaders().add("Cookie", cookie);
        return request;
    }

    private String getSessionCookie(String url) {
        if (StringUtils.isEmpty(url)) return "";

        List<String> cookies = new RestTemplate()
                .getForEntity(url, String.class)
                .getHeaders()
                .get("Set-Cookie");

        return cookies.stream().findFirst().orElse("");
    }

    public CookieRestTemplate withSessionInitUrl(String sessionInitUrl){
        if (StringUtils.isNotEmpty(sessionInitUrl)) this.sessionInitUrl = sessionInitUrl;
        return this;
    }
}
