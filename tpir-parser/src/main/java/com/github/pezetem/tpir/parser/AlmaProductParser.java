package com.github.pezetem.tpir.parser;

import com.github.pezetem.tpir.domain.json.JsonAlmaProduct;
import com.github.pezetem.tpir.domain.utils.DomainUtils;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.github.pezetem.tpir.parser.utils.JsonStreamUtils;
import com.github.pezetem.tpir.domain.HistoricalProduct;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class AlmaProductParser implements ProductParser {

    @Override
    public List<HistoricalProduct> parseFile(String fileName) {
        JsonReader jsonReader = JsonStreamUtils.readJsonStream(fileName);
        return readJsonFile(jsonReader);
    }

    @Override
    public List<HistoricalProduct> parseFile(File fileName) {
        Optional<JsonReader> optionalReader = JsonStreamUtils.readJsonStream(fileName);
        JsonReader reader = optionalReader.get();
        return readJsonFile(reader);
    }

    private List<HistoricalProduct> readJsonFile(JsonReader reader) {
        List<JsonAlmaProduct> jsonProducts = parseAlmaProductFromJson(reader);
        return DomainUtils.convertToProductDomain(jsonProducts);
    }

    private List<JsonAlmaProduct> parseAlmaProductFromJson(JsonReader reader) {
        List<JsonAlmaProduct> jsonProducts = new ArrayList<>();
        try {
            reader.beginObject();
            jsonProducts = parseMainBody(reader);
            reader.endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonProducts;
    }

    private List<JsonAlmaProduct> parseMainBody(JsonReader reader) throws IOException {
        List<JsonAlmaProduct> jsonAlmaProducts = new ArrayList<>();
        while (reader.hasNext()) {
            String jsonField = reader.nextName();
            if ("content".equals(jsonField)) {
                reader.beginArray();
                jsonAlmaProducts.addAll(parseProductList(reader));
                reader.endArray();
            }
            JsonStreamUtils.skipValue(reader, "totalCount", jsonField);
            JsonStreamUtils.skipValue(reader, "stampsIds", jsonField);
            JsonStreamUtils.skipValue(reader, "promotionsIds", jsonField);
            JsonStreamUtils.skipValue(reader, "rootCategoriesIds", jsonField);
            JsonStreamUtils.skipValue(reader, "attributeFacets", jsonField);
            JsonStreamUtils.skipValue(reader, "brandFacets", jsonField);
            JsonStreamUtils.skipValue(reader, "fuzzy", jsonField);
            JsonStreamUtils.skipValue(reader, "totalPages", jsonField);
        }
        return jsonAlmaProducts;
    }

    private List<JsonAlmaProduct> parseProductList(JsonReader reader) throws IOException {
        List<JsonAlmaProduct> jsonProducts = new ArrayList<>();
        while (reader.hasNext()) {
            JsonAlmaProduct jsonProduct = new Gson().fromJson(reader, JsonAlmaProduct.class);
            jsonProducts.add(jsonProduct);
        }
        return jsonProducts;
    }
}
