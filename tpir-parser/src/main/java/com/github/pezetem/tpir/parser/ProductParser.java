package com.github.pezetem.tpir.parser;

import com.github.pezetem.tpir.domain.HistoricalProduct;

import java.io.File;
import java.util.List;

public interface ProductParser {
    List<HistoricalProduct> parseFile(String fileName);
    List<HistoricalProduct> parseFile(File file);
}
