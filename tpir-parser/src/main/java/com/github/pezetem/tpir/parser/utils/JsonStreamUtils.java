package com.github.pezetem.tpir.parser.utils;

import com.google.gson.stream.JsonReader;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Optional;

public class JsonStreamUtils {

    public static JsonReader readJsonStream(String fileContent){
        return new JsonReader(new InputStreamReader(IOUtils.toInputStream(fileContent)));
    }

    public static Optional<JsonReader> readJsonStream(File jsonFile) {
        try {
            return Optional.of(new JsonReader(
                    new InputStreamReader(
                            new FileInputStream(jsonFile), Charset.defaultCharset()
                    )
            ));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static void skipValue(JsonReader reader, String jsonField) throws IOException {
        if (jsonField.equals(reader.nextName())){
            reader.skipValue();
        }
    }

    public static void skipValue(JsonReader reader, String expectedJsonField, String actualJsonField) throws IOException {
        if (expectedJsonField.equals(actualJsonField)){
            reader.skipValue();
        }
    }

    public static String skipOptionalName(JsonReader reader, String expectedJsonField) throws IOException {
        String actualJsonField = reader.nextName();
        if (expectedJsonField.equals(actualJsonField)){
            reader.skipValue();
            return reader.nextName();
        }
        return actualJsonField;
    }

    public static double readDouble(JsonReader reader, String jsonField) throws IOException {
        if (jsonField.equals(reader.nextName())){
            return reader.nextDouble();
        }
        return 0;
    }

    public static String readString(JsonReader reader, String jsonField) throws IOException {
        if (jsonField.equals(reader.nextName()))
            return reader.nextString();
        return "";
    }
}
