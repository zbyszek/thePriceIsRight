package com.github.pezetem.tpir.parser;

import com.github.pezetem.tpir.domain.HistoricalProduct;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class TescoProductParser implements ProductParser {

    public static final String RIGHT_PARATHESIS = "\\)";
    public static final String SLASH = "/";

    @Override
    public List<HistoricalProduct> parseFile(File file) {
        Optional<Document> parsedDocument = parseDocument(file);
        return extractProducts(parsedDocument.orElse(new Document("")));
    }

    private Optional<Document> parseDocument(File file) {
        try {
            return Optional.of(Jsoup.parse(file, "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public List<HistoricalProduct> parseFile(String pageContent) {
        Document parsedPage = Jsoup.parse(pageContent);
        return extractProducts(parsedPage);
    }

    private List<HistoricalProduct> extractProducts(Document parsedPage) {
        return Arrays.asList(".firstProduct", ".secondProduct", ".thirdProduct")
                .stream()
                .map(e -> parsePage(parsedPage, e))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<HistoricalProduct> parsePage(Document parsedPage, String productCssClass) {
        return parsedPage
                .select(productCssClass)
                .stream()
                .map(this::mapToProduct)
                .collect(Collectors.toList());
    }

    private HistoricalProduct mapToProduct(Element htmlElement) {
        HistoricalProduct historicalProduct = new HistoricalProduct();

        historicalProduct.setGrammage(new BigDecimal("-1"));
        historicalProduct.setGrammagePerUnit(parseGrammagePerUnit(htmlElement));
        historicalProduct.setName(parseProductTitle(htmlElement));
        historicalProduct.setPrice(parsePrice(htmlElement));
        historicalProduct.setInternalShopId(parseInternalProductId(htmlElement));
        historicalProduct.setProductUrl(parseProductUrl(htmlElement));
        historicalProduct.setUnitSell(parseUnitSell(htmlElement));
        historicalProduct.setUnitSize(parseUnitSize(htmlElement));
        historicalProduct.setShopName("Tesco");
        // TODO find a way to get img url
        // historicalProduct.setImgUrl();

        return historicalProduct;
    }

    private String parseProductTitle(Element htmlElement) {
        String title = Optional.of(htmlElement.select("h2 > a").attr("title")).orElse("");
        return title.replaceAll("\u00a0", " ");
    }

    private String parseInternalProductId(Element htmlElement) {
        Elements internalId = htmlElement.select("input");
        if (internalId.isEmpty()) return "0";
        return Optional.of(
                        internalId.get(0)
                                .attr("value"))
                        .orElse("0");
    }

    private String parseProductUrl(Element htmlElement) {
        return Optional.of(htmlElement.select("h2 > a").attr("href")).orElse("");
    }

    private String parseUnitSize(Element htmlElement) {
        return Optional.of(
                htmlElement.select(".linePriceAbbr")
                        .text()
                        .split(SLASH)[1]
                        .split(RIGHT_PARATHESIS)[0])
                .orElse("");
    }

    private String parseUnitSell(Element htmlElement) {
        String defaultSellingUnit = htmlElement.select(".textbox").attr("value");
        if (!"1".equals(defaultSellingUnit)) {
            return defaultSellingUnit + " g";
        }
        return parseUnitFromProductTitle(htmlElement);
    }

    private String parseUnitFromProductTitle(Element htmlElement) {
        String productDescription = parseProductTitle(htmlElement);
        String[] splittedProductDescr = productDescription.split(" ");
        if (splittedProductDescr.length < 3) return "1 szt.";
        if (!"gl".contains(splittedProductDescr[splittedProductDescr.length - 1])) return "1 szt.";
        return splittedProductDescr[splittedProductDescr.length - 2]
                + " "
                + splittedProductDescr[splittedProductDescr.length - 1];
    }

    private String parseGrammagePerUnit(Element htmlElement) {
        String grammagePerUnitRaw = htmlElement.select(".linePriceAbbr").text();
        return grammagePerUnitRaw.substring(1, grammagePerUnitRaw.length() - 1);
    }

    private BigDecimal parsePrice(Element htmlElement) {
        String priceRaw = Optional.of(htmlElement.select(".linePrice").text()).orElse("0");
        String[] splitPriceOnCurrency = priceRaw.split(" ");
        if (splitPriceOnCurrency.length == 0) return new BigDecimal("0");
        return new BigDecimal(splitPriceOnCurrency[0].replace(",", "."));
    }
}
