package com.github.pezetem.tpir.parser;

import org.apache.commons.io.FileUtils;
import org.assertj.core.api.Assertions;
import com.github.pezetem.tpir.domain.HistoricalProduct;
import org.junit.Test;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.util.List;

public class TescoProductParserTest {

    private static ResourceLoader RESOURCE_LOADER = new DefaultResourceLoader();

    @Test
    public void shouldParsePage() throws IOException {
        Resource resource = RESOURCE_LOADER.getResource("pages/tescoPage.html");
        List<HistoricalProduct> historicalProductList = new TescoProductParser().parseFile(FileUtils.readFileToString(resource.getFile()));

        Assertions.assertThat(historicalProductList).isNotEmpty();
    }
}