package com.github.pezetem.tpir.parser

import com.github.pezetem.tpir.domain.HistoricalProduct
import spock.lang.Ignore
import spock.lang.Specification

class AlmaProductParserTest extends Specification {

    @Ignore
    def "should parse alma json"() {
        def fileName = ""
        given:
        def jsonFile = new File(fileName);

        when:
        List<HistoricalProduct> products = new AlmaProductParser().parseFile(jsonFile);

        then:
        products.size()
    }
}
