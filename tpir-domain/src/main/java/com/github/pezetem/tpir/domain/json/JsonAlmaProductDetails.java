package com.github.pezetem.tpir.domain.json;

import com.google.gson.annotations.SerializedName;
import lombok.Value;

@Value
public class JsonAlmaProductDetails {
    @SerializedName("sizeWithUnitString")
    private String unitSize;

    @SerializedName("sellUnitString")
    private String unitSell;
}
