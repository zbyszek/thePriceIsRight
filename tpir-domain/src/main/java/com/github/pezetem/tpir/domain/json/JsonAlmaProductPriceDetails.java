package com.github.pezetem.tpir.domain.json;

import com.google.gson.annotations.SerializedName;
import lombok.Value;


@Value
public class JsonAlmaProductPriceDetails {
    @SerializedName("actualGrossPrice")
    private String price;
}
