package com.github.pezetem.tpir.domain;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Builder
@Getter
@ToString
@EqualsAndHashCode
public class Product {
    private String name;
    private String shopName;
    private String productUrl;
    private String unitSize;
    private String unitSell;
    private BigDecimal price;
    private String grammagePerUnit;
    private String imgUrl;
}