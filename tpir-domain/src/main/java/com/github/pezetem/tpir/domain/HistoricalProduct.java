package com.github.pezetem.tpir.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "product")
@Data
@EqualsAndHashCode(exclude = "productId")
@ToString
@Audited
public class HistoricalProduct {
    private static final long DEFAULT_VALUE = -1L;
    private static final String EMPTY_STRING = "";

    @Id
    @SequenceGenerator(allocationSize = 1, sequenceName = "product_product_id_seq", name = "product_product_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_product_id_seq")
    @Column(name = "product_id")
    private Long productId;

    @Column(name = "name")
    private String name = EMPTY_STRING;

    @Column(name = "internal_shop_id")
    private String internalShopId = EMPTY_STRING;

    @Column(name = "shop_name")
    private String shopName = EMPTY_STRING;

    @Column(name = "product_url")
    private String productUrl = EMPTY_STRING;

    @Column(name = "unit_size")
    private String unitSize = EMPTY_STRING;

    @Column(name = "unit_sell")
    private String unitSell = EMPTY_STRING;

    @Column(name = "actual_gross_price")
    private BigDecimal price = new BigDecimal(DEFAULT_VALUE);

    @Column(name = "grammage")
    private BigDecimal grammage = new BigDecimal(DEFAULT_VALUE);

    @Column(name = "grammage_unit")
    private String grammagePerUnit = EMPTY_STRING;

    @Column(name = "img_url")
    private String imgUrl = EMPTY_STRING;
}
