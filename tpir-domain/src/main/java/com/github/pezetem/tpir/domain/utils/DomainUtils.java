package com.github.pezetem.tpir.domain.utils;

import com.github.pezetem.tpir.domain.HistoricalProduct;
import com.github.pezetem.tpir.domain.json.*;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

public class DomainUtils {
    public static final String ALMA_SHOP_NAME = "Alma";

    public static List<HistoricalProduct> convertToProductDomain(List<JsonAlmaProduct> jsonProducts) {
        return jsonProducts.stream()
                .map(json -> {
                    String productName = json.getName();
                    String productUrl = json.getProductUrl();
                    String internalShopId = json.getInternalShopId();

                    JsonAlmaProductDetails productDetails = json.getJsonAlmaProductDetails();
                    String unitSell = productDetails.getUnitSell();
                    String unitSize = productDetails.getUnitSize();

                    JsonAlmaProductImage productImage = json.getJsonAlmaProductImage();
                    String imgUrl = productImage.getImgUrl();

                    JsonAlmaProductPrice productPrice = json.getJsonAlmaProductPrice();
                    String grammagePerUnit = productPrice.getGrammagePerUnit();

                    JsonAlmaProductPriceDetails productPriceDetails = productPrice.getJsonAlmaProductPriceDetails();
                    BigDecimal price = convertToBigDecimal(productPriceDetails);

                    return createProduct(productName, productUrl, internalShopId, unitSell, unitSize, imgUrl, grammagePerUnit, price);
                }).collect(Collectors.toList());
    }

    private static BigDecimal convertToBigDecimal(JsonAlmaProductPriceDetails productPriceDetails) {
        return new BigDecimal(
                                productPriceDetails.getPrice(),
                                new MathContext(3, RoundingMode.HALF_EVEN)
                        );
    }

    private static HistoricalProduct createProduct(String productName, String productUrl, String internalShopId, String unitSell, String unitSize, String imgUrl, String grammagePerUnit, BigDecimal price) {
        HistoricalProduct product = new HistoricalProduct();
        product.setName(productName);
        product.setInternalShopId(internalShopId);
        product.setShopName(ALMA_SHOP_NAME);
        product.setProductUrl(productUrl);
        product.setUnitSell(unitSell);
        product.setUnitSize(unitSize);
        product.setPrice(price);
        product.setGrammagePerUnit(grammagePerUnit);
        product.setImgUrl(imgUrl);
        return product;
    }
}
