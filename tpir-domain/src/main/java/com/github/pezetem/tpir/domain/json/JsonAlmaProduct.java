package com.github.pezetem.tpir.domain.json;

import com.google.gson.annotations.SerializedName;
import lombok.Value;

@Value
public class JsonAlmaProduct {
    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private String internalShopId;

    @SerializedName("url")
    private String productUrl;

    @SerializedName("product")
    private JsonAlmaProductDetails jsonAlmaProductDetails;

    @SerializedName("actualSku")
    private JsonAlmaProductPrice jsonAlmaProductPrice;

    @SerializedName("defaultImage")
    private JsonAlmaProductImage jsonAlmaProductImage;
}
