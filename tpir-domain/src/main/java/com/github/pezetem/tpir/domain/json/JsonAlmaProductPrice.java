package com.github.pezetem.tpir.domain.json;

import com.google.gson.annotations.SerializedName;
import lombok.Value;

@Value
public class JsonAlmaProductPrice {
    @SerializedName("grammageWithUnitString")
    private String grammagePerUnit;

    @SerializedName("amount")
    private JsonAlmaProductPriceDetails jsonAlmaProductPriceDetails;
}
