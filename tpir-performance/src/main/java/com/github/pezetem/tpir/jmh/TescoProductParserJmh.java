package com.github.pezetem.tpir.jmh;

import com.github.pezetem.tpir.domain.HistoricalProduct;
import com.github.pezetem.tpir.parser.TescoProductParser;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@State(Scope.Benchmark)
public class TescoProductParserJmh {
    private static final String JFR_OPTS_TEMPLATE = "-XX:FlightRecorderOptions=defaultrecording=true,settings=profile.jfc,dumponexit=true,dumponexitpath=";
    @Param("default/pathname")
    public String PATHNAME;

    @Fork(value = 1, jvmArgs = JFR_OPTS_TEMPLATE + "test.jfr")
    @Benchmark
    public List<HistoricalProduct> parseTescoPage() throws IOException {
        File[] dataFiles = new File(PATHNAME).listFiles();
        if(dataFiles == null) return new ArrayList<>();

        TescoProductParser productParser = new TescoProductParser();

        return Stream.of(dataFiles)
                .parallel()
                .map(productParser::parseFile)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) throws RunnerException {
        if (args.length < 1){
            throw new IllegalArgumentException("Missing resource directory");
        }

        Options opt = new OptionsBuilder()
                .include(TescoProductParserJmh.class.getSimpleName())
                .warmupIterations(10)
                .measurementIterations(30)
                .resultFormat(ResultFormatType.CSV)
                .jvmArgsPrepend("-XX:+UnlockCommercialFeatures", "-XX:+FlightRecorder")
                .param("PATHNAME", args[0])
                .build();

        new Runner(opt).run();
    }
}
