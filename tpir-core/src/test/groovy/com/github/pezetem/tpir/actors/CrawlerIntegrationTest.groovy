package com.github.pezetem.tpir.actors

import com.github.pezetem.tpir.ThePriceIsRightApplication
import com.github.pezetem.tpir.controller.CrawlerController
import com.github.pezetem.tpir.db.service.HistoricalProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.util.concurrent.PollingConditions

@ContextConfiguration(loader = SpringApplicationContextLoader, classes = ThePriceIsRightApplication.class)
class CrawlerIntegrationTest extends Specification {

    @Autowired
    CrawlerController controller

    @Autowired
    HistoricalProductService productService

    def "tesco eshop crawler integration test"() {
        given:
        def conditions = new PollingConditions(timeout: 300, initialDelay: 5)
        String pathUrl = "pages/tesco/tescoUrls.txt"
        when:
        controller.startCrawlingTesco(pathUrl)
        then:
        conditions.eventually {
            assert productService.countAllProducts() > 16000
        }
    }

    def "alma eshop crawler integration test"() {
        given:
        def conditions = new PollingConditions(timeout: 300, initialDelay: 5)
        String pathUrl = "pages/alma/almaUrls.txt"
        when:
        controller.startCrawlingAlma(pathUrl)
        then:
        conditions.eventually {
            assert productService.countAllProducts() > 9000
        }
    }
}
