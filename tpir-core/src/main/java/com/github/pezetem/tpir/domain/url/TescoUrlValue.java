package com.github.pezetem.tpir.domain.url;

import lombok.Value;

@Value
public class TescoUrlValue implements UrlValue {
    private String url;
}
