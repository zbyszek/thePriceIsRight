package com.github.pezetem.tpir.actors;

import akka.actor.UntypedActor;
import com.github.pezetem.tpir.domain.HistoricalProduct;
import com.github.pezetem.tpir.db.service.HistoricalProductService;
import com.github.pezetem.tpir.domain.product.ProductValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import java.util.List;

@Component("StorageActor")
@Scope("prototype")
@Slf4j
public class StorageActor extends UntypedActor{
    private HistoricalProductService productService;

    @Autowired
    public StorageActor(HistoricalProductService productService) {
        this.productService = productService;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof ProductValue){
            List<HistoricalProduct> productList = ((ProductValue) message).getProductList();
            String url = ((ProductValue) message).getUrl();
            log.info("storing {} products from {} url", productList.size(), url);
            productService.saveProduct(productList);
            return;
        }
        throw new IllegalStateException("Parsed product is not valid " + message.toString());
    }
}
