package com.github.pezetem.tpir.scheduler;

import com.github.pezetem.tpir.controller.CrawlerController;
import com.github.pezetem.tpir.utils.TimeConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

@Component
@Slf4j
public class CrawlerScheduler {
    @Value("${crawling.url.tesco}")
    private String tescoUrlFilePath;

    @Value("${crawling.url.alma}")
    private String almaUrlFilePath;

    private CrawlerController crawlerController;

    @Autowired
    public CrawlerScheduler(CrawlerController crawlerController) {
        this.crawlerController = crawlerController;
    }

    @Scheduled(cron = TimeConstant.EVERY_DAY_AT_MIDNIGHT)
    public void crawlTesco() throws IOException {
        log.info("Started crawling Tesco shop at {}", LocalDateTime.now());
        crawlerController.startCrawlingTesco(tescoUrlFilePath);
    }

    @Scheduled(cron = TimeConstant.EVERY_DAY_AT_MIDNIGHT)
    public void crawlAlma() throws IOException {
        log.info("Started crawling Alma shop at {}", LocalDateTime.now());
        crawlerController.startCrawlingAlma(almaUrlFilePath);
    }
}
