package com.github.pezetem.tpir.actors;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.UntypedActor;
import com.github.pezetem.tpir.domain.HistoricalProduct;
import com.github.pezetem.tpir.domain.page.AlmaPageValue;
import com.github.pezetem.tpir.domain.page.TescoPageValue;
import com.github.pezetem.tpir.domain.product.ProductValue;
import com.github.pezetem.tpir.parser.AlmaProductParser;
import com.github.pezetem.tpir.parser.TescoProductParser;
import com.github.pezetem.tpir.utils.SpringExtension;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component("ParserActor")
@Scope("prototype")
public class ParserActor extends UntypedActor {
    private TescoProductParser tescoProductParser;
    private AlmaProductParser almaProductParser;
    ActorRef storageActor;

    @Autowired
    public ParserActor(TescoProductParser tescoProductParser,
                       AlmaProductParser almaProductParser,
                       @Qualifier("actorSystem") ActorSystem actorSystem) {
        this.tescoProductParser = tescoProductParser;
        this.almaProductParser = almaProductParser;
        storageActor = context().actorOf(
                SpringExtension
                        .SpringExtProvider
                        .get(actorSystem)
                        .props("StorageActor"),
                "storingActor");
    }

    @Override
    public void onReceive(Object message) throws Exception {
        List<HistoricalProduct> products;
        String url;
        if (message instanceof TescoPageValue) {
            String page = ((TescoPageValue) message).getPage();
            url = ((TescoPageValue) message).getUrl();
            products = tescoProductParser.parseFile(page);
        } else if (message instanceof AlmaPageValue) {
            String page = ((AlmaPageValue) message).getPage();
            url = ((AlmaPageValue) message).getUrl();
            products = almaProductParser.parseFile(page);
        } else {
            throw new IllegalStateException("Shop parsed page is not valid " + message.toString());
        }
        log.info("parsed {} products from {} url", products.size(), url);
        storageActor.tell(new ProductValue(products, url), sender());
    }
}
