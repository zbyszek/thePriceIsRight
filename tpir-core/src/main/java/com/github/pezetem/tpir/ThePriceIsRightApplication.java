package com.github.pezetem.tpir;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ThePriceIsRightApplication {

    public static void main(String[] args){
        new SpringApplicationBuilder()
                .sources(ThePriceIsRightApplication.class)
                .run(args);
    }
}
