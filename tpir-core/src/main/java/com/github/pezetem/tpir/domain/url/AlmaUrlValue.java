package com.github.pezetem.tpir.domain.url;

import lombok.Value;

@Value
public class AlmaUrlValue implements UrlValue {
    private String url;
}
