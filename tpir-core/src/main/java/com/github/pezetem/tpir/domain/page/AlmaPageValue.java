package com.github.pezetem.tpir.domain.page;

import lombok.Value;

@Value
public class AlmaPageValue implements PageValue {
    private String page;
    private String url;
}
