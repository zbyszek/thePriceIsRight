package com.github.pezetem.tpir.controller;

import akka.actor.ActorRef;
import com.github.pezetem.tpir.domain.url.AlmaUrlValue;
import com.github.pezetem.tpir.domain.url.TescoUrlValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Component
public class CrawlerController {
    private DefaultResourceLoader RESOURCE_LOADER = new DefaultResourceLoader();
    private ActorRef tescoCrawlerActor;
    private ActorRef almaCrawlerActor;

    @Autowired
    public CrawlerController(@Qualifier("tescoCrawler") ActorRef tescoCrawlerActor,
                             @Qualifier("almaCrawler") ActorRef almaCrawlerActor) {
        this.tescoCrawlerActor = tescoCrawlerActor;
        this.almaCrawlerActor = almaCrawlerActor;
    }


    public void startCrawlingTesco(String resourcePath) throws IOException {
        Resource resource = RESOURCE_LOADER.getResource(resourcePath);
        File file = resource.getFile();
        Files.lines(file.toPath()).forEach(url -> tescoCrawlerActor.tell(new TescoUrlValue(url), ActorRef.noSender()));
    }

    public void startCrawlingAlma(String resourcePath) throws IOException {
        Resource resource = RESOURCE_LOADER.getResource(resourcePath);
        File file = resource.getFile();
        Files.lines(file.toPath()).forEach(url -> almaCrawlerActor.tell(new AlmaUrlValue(url), ActorRef.noSender()));
    }
}
