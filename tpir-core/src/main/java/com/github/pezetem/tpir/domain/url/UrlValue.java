package com.github.pezetem.tpir.domain.url;

public interface UrlValue {
    String getUrl();
}
