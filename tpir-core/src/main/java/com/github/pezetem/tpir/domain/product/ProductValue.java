package com.github.pezetem.tpir.domain.product;

import com.github.pezetem.tpir.domain.HistoricalProduct;
import lombok.Value;

import java.util.List;

@Value
public class ProductValue {
    private List<HistoricalProduct> productList;
    private String url;
}
