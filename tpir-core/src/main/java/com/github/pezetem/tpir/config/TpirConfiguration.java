package com.github.pezetem.tpir.config;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.RoundRobinPool;
import com.github.pezetem.tpir.utils.SpringExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TpirConfiguration {
    @Autowired
    private ApplicationContext applicationContext;

    @Bean(name = "tescoCrawler")
    public ActorRef getTescoCrawlerActor() {
        return getActorSystem().actorOf(
                new RoundRobinPool(10).props(
                        SpringExtension
                                .SpringExtProvider
                                .get(getActorSystem())
                                .props("CrawlerActor")),
                "tpir-tesco-crawler");
    }

    @Bean(name = "almaCrawler")
    public ActorRef getAlmaCrawlerActor() {
        return getActorSystem().actorOf(
                new RoundRobinPool(10).props(
                        SpringExtension
                                .SpringExtProvider
                                .get(getActorSystem())
                                .props("CrawlerActor")),
                "tpir-alma-crawler");
    }

    @Bean(name = "actorSystem")
    public ActorSystem getActorSystem() {
        ActorSystem actorSystem = ActorSystem.create("the-price-is-right");
        SpringExtension.SpringExtProvider
                .get(actorSystem)
                .initialize(applicationContext);
        return actorSystem;
    }
}
