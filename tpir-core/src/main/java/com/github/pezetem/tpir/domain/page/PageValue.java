package com.github.pezetem.tpir.domain.page;

public interface PageValue {
    String getPage();
    String getUrl();
}
