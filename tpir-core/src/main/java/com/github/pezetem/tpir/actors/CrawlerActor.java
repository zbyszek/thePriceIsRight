package com.github.pezetem.tpir.actors;

import akka.actor.*;
import akka.japi.pf.DeciderBuilder;
import com.github.pezetem.tpir.crawler.AlmaEShopCrawler;
import com.github.pezetem.tpir.crawler.TescoEShopCrawler;
import com.github.pezetem.tpir.domain.page.AlmaPageValue;
import com.github.pezetem.tpir.domain.page.TescoPageValue;
import com.github.pezetem.tpir.domain.url.AlmaUrlValue;
import com.github.pezetem.tpir.domain.url.TescoUrlValue;
import com.github.pezetem.tpir.utils.SpringExtension;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import scala.concurrent.duration.Duration;

import java.util.List;

@Slf4j
@Component("CrawlerActor")
@Scope("prototype")
public class CrawlerActor extends UntypedActor {
    private AlmaEShopCrawler almaCrawler;
    private TescoEShopCrawler tescoCrawler;
    private ActorRef parserActor;

    @Autowired
    public CrawlerActor(AlmaEShopCrawler almaCrawler,
                        TescoEShopCrawler tescoCrawler,
                        @Qualifier("actorSystem") ActorSystem actorSystem) {
        this.almaCrawler = almaCrawler;
        this.tescoCrawler = tescoCrawler;
        parserActor = context().actorOf(
                SpringExtension
                        .SpringExtProvider
                        .get(actorSystem)
                        .props("ParserActor"),
                "parsingActor");
    }

    @Override
    public void onReceive(Object message) throws Exception {
        List<String> downloadedPages;
        if (message instanceof TescoUrlValue) {
            String url = ((TescoUrlValue) message).getUrl();
            log.info("paring page {}", url);

            downloadedPages = tescoCrawler.downloadHtmlPageWithSubpages(url);
            downloadedPages.forEach(page -> parserActor.tell(new TescoPageValue(page, url), sender()));
            return;
        } else if (message instanceof AlmaUrlValue) {
            String url = ((AlmaUrlValue) message).getUrl();
            log.info("paring page {}", url);

            downloadedPages = almaCrawler.downloadHtmlPageWithSubpages(url);
            downloadedPages.forEach(page -> parserActor.tell(new AlmaPageValue(page, url), sender()));
            return;
        }
        throw new IllegalStateException("Url is not valid " + message.toString());
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return new AllForOneStrategy(2, Duration.create("1 minute"),
                DeciderBuilder
                        .match(Exception.class, e -> SupervisorStrategy.stop())
                        .build());
    }
}
