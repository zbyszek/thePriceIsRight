package com.github.pezetem.tpir.mvc

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.github.pezetem.tpir")
class MvcApplicationTest {
    public static void main(String[] args){
        SpringApplication.run(MvcApplicationTest.class, args);
    }
}
