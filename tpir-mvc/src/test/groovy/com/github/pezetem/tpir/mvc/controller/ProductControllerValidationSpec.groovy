package com.github.pezetem.tpir.mvc.controller

import com.github.pezetem.tpir.mvc.MvcApplicationTest
import com.github.pezetem.tpir.mvc.dto.request.ProductRequest
import com.google.gson.Gson
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

@ContextConfiguration(loader = SpringApplicationContextLoader, classes = MvcApplicationTest.class)
@WebAppConfiguration
class ProductControllerValidationSpec extends Specification {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    def setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    def "should return internal server error status for incorrect input parameters"() {
        expect:
        MockHttpServletResponse servletResponse = performGetRequest(productName, shopName)
        servletResponse.contentAsString.equals(restResponse)
        servletResponse.getStatus() == status

        where:
        productName | shopName | restResponse                                                       | status
        ""          | ""       | "{\"errors\":[\"product name is empty\",\"shop is not allowed\"]}" | 500
        "Mleko"     | ""       | "{\"errors\":[\"shop is not allowed\"]}"                           | 500
        ""          | "Alma"   | "{\"errors\":[\"product name is empty\"]}"                         | 500
    }

    private MockHttpServletResponse performGetRequest(String productName, String shopName) {
        String requestProduct = new Gson().toJson(new ProductRequest(productName, shopName));
        return mockMvc.perform(MockMvcRequestBuilders
                .get("/products")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(requestProduct))
                .andReturn()
                .getResponse()
    }
}
