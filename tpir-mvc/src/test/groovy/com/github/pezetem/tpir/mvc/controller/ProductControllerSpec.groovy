package com.github.pezetem.tpir.mvc.controller

import com.github.pezetem.tpir.domain.Product
import com.github.pezetem.tpir.es.service.ProductService
import com.github.pezetem.tpir.mvc.MvcApplicationTest
import com.github.pezetem.tpir.mvc.dto.request.ProductRequest
import com.google.gson.Gson
import org.mockito.InjectMocks
import org.mockito.Matchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

@ContextConfiguration(loader = SpringApplicationContextLoader, classes = MvcApplicationTest.class)
@WebAppConfiguration
class ProductControllerSpec extends Specification{

    @InjectMocks
    private ProductController productController;
    @Mock
    private ProductService productService;
    private MockMvc mockMvc;

    def setup(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
        Mockito.when(productService.
                findProductByName(
                        Matchers.anyString(),
                        Matchers.anyString()))
                .thenReturn(
                Arrays.asList(
                        Product.builder()
                                .shopName("shopName")
                                .name("productName")
                                .build()));
    }

    def "should return status 200 for correct input arguments"(){
        given:
            String mockedShopName = "shopName"
            String mockedProductName = "productName"
        when:
            MockHttpServletResponse servletResponse = performGetRequest(mockedShopName, mockedProductName)
        then:
            servletResponse.contentAsString.equals("[{\"name\":\"productName\",\"shopName\":\"shopName\"}]")
            servletResponse.getStatus() == 200
    }

    private MockHttpServletResponse performGetRequest(String productName, String shopName) {
        String requestProduct = new Gson().toJson(new ProductRequest(productName, shopName));
        return mockMvc.perform(MockMvcRequestBuilders
                .get("/products")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(requestProduct))
                .andReturn()
                .getResponse()
    }
}
