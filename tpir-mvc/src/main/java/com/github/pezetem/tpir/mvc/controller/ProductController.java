package com.github.pezetem.tpir.mvc.controller;

import com.github.pezetem.tpir.domain.Product;
import com.github.pezetem.tpir.es.service.ProductService;
import com.github.pezetem.tpir.mvc.dto.request.ProductRequest;
import com.github.pezetem.tpir.mvc.dto.response.ProductResponse;
import com.github.pezetem.tpir.mvc.exception.ValidationException;
import com.github.pezetem.tpir.mvc.validator.ProductRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductController {
    private ProductRequestValidator productRequestValidator;
    private ProductService productService;

    @InitBinder
    public void initBinder(WebDataBinder binder){
        binder.addValidators(productRequestValidator);
    }

    @Autowired
    public ProductController(ProductRequestValidator productRequestValidator, ProductService productService) {
        this.productRequestValidator = productRequestValidator;
        this.productService = productService;
    }

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public List<ProductResponse> retrieveProducts(@Valid @RequestBody ProductRequest requestProduct, BindingResult bindingResult)
            throws ValidationException {
        if (bindingResult.hasErrors()){
            throw new ValidationException(bindingResult);
        }

        List<Product> products = productService.findProductByName(requestProduct.getProductName(), requestProduct.getShopName());

        return products.parallelStream().map(p -> ProductResponse.builder()
                .name(p.getName())
                .imgUrl(p.getImgUrl())
                .price(p.getPrice())
                .productUrl(p.getProductUrl())
                .shopName(p.getShopName())
                .unitSize(p.getUnitSize())
                .build())
                .collect(Collectors.toList());
    }
}
