package com.github.pezetem.tpir.mvc.exception;

import lombok.Getter;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class ValidationException extends Exception {
    private List<String> validationMessage;

    public ValidationException(BindingResult bindingResult) {
        this.validationMessage = getErrorMessage(bindingResult.getAllErrors());
    }

    private List<String> getErrorMessage(List<ObjectError> bindingResult) {
        return bindingResult.stream()
                .map(ObjectError::getDefaultMessage)
                .collect(Collectors.toList());
    }
}
