package com.github.pezetem.tpir.mvc.validator;

import com.github.pezetem.tpir.mvc.dto.request.ProductRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Arrays;
import java.util.List;

@Component
public class ProductRequestValidator implements Validator {
    private static List<String> ALLOWED_SHOP = Arrays.asList("Alma", "Tesco");

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(ProductRequest.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ProductRequest productRequest = (ProductRequest) o;
        validateProductName(productRequest.getProductName(), errors);
        validateShopName(productRequest.getShopName(), errors);
    }

    private void validateProductName(String productName, Errors errors) {
        if (StringUtils.isEmpty(productName)){
            errors.reject("product.name", "product name is empty");
        }
    }

    private void validateShopName(String shopName, Errors errors) {
        if (ALLOWED_SHOP.contains(shopName)) {
            return;
        }
        errors.reject("shop.name", "shop is not allowed");
    }
}
