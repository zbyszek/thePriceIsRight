package com.github.pezetem.tpir.mvc.dto.request;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@EqualsAndHashCode
public class ProductRequest {
    private String productName;
    private String shopName;
}
