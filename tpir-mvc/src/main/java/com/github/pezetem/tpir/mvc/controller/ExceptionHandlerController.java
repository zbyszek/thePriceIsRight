package com.github.pezetem.tpir.mvc.controller;

import com.github.pezetem.tpir.mvc.dto.response.ErrorResponse;
import com.github.pezetem.tpir.mvc.exception.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(value = ValidationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ErrorResponse throwValidationException(ValidationException e){
        return new ErrorResponse(e.getValidationMessage());
    }
}
