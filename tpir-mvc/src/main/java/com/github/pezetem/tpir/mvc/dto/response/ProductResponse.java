package com.github.pezetem.tpir.mvc.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import java.math.BigDecimal;

@Builder
@Getter
@AllArgsConstructor
@EqualsAndHashCode
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ProductResponse {
    private String name;
    private String shopName;
    private String productUrl;
    private String unitSize;
    private BigDecimal price;
    private String imgUrl;
}
