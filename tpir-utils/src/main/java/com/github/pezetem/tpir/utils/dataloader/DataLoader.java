package com.github.pezetem.tpir.utils.dataloader;

import com.github.pezetem.tpir.db.service.HistoricalProductService;
import com.github.pezetem.tpir.parser.AlmaProductParser;
import com.github.pezetem.tpir.parser.TescoProductParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.github.pezetem.tpir.db")
public class DataLoader {

    public static void main(String[] args){
        ConfigurableApplicationContext run = SpringApplication.run(DataLoader.class);
        HistoricalProductService historicalProductService = run.getBean(HistoricalProductService.class);

        String dataDirectory = "/home/akfaz/projects/java/thePriceIsRight/data/alma/2015-11-14";

        new DbUtils(historicalProductService, new AlmaProductParser())
                .uploadProductsFromDirectory(dataDirectory);

        dataDirectory = "/home/akfaz/projects/java/thePriceIsRight/data/tesco/2015-12-26";

        new DbUtils(historicalProductService, new TescoProductParser())
                .uploadProductsFromDirectory(dataDirectory);
    }
}
