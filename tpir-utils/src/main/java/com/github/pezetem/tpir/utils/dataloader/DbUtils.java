package com.github.pezetem.tpir.utils.dataloader;

import com.github.pezetem.tpir.db.domain.HistoricalProduct;
import com.github.pezetem.tpir.db.service.HistoricalProductService;
import com.github.pezetem.tpir.parser.ProductParser;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DbUtils {
    private HistoricalProductService historicalProductService;
    private ProductParser productParser;

    public DbUtils(HistoricalProductService historicalProductService, ProductParser productParser) {
        this.historicalProductService = historicalProductService;
        this.productParser = productParser;
    }

    public void uploadProductsFromDirectory(String dataDirectory) {
        File[] dataFiles = new File(dataDirectory).listFiles();
        List<HistoricalProduct> historicalProducts = Stream.of(dataFiles)
                .map(productParser::parseFile)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        historicalProductService.saveProduct(historicalProducts);
    }
}
