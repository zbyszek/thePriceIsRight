# The price is right

[![Build Status](https://travis-ci.org/pezetem/thePriceIsRight.svg?branch=master)](https://travis-ci.org/pezetem/thePriceIsRight)
[![Coverage Status](https://coveralls.io/repos/github/pezetem/thePriceIsRight/badge.svg?branch=master)](https://coveralls.io/github/pezetem/thePriceIsRight?branch=master)

Compare cost of your grocery list between shops.
