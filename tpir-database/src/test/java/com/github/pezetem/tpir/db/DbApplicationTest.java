package com.github.pezetem.tpir.db;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbApplicationTest {
    public static void main(String[] args){
        SpringApplication.run(DbApplicationTest.class, args);
    }
}
