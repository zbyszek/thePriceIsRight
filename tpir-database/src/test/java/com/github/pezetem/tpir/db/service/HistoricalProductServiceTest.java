package com.github.pezetem.tpir.db.service;

import org.assertj.core.api.Assertions;
import com.github.pezetem.tpir.db.DbApplicationTest;
import com.github.pezetem.tpir.domain.HistoricalProduct;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.history.Revisions;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.math.BigDecimal;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DbApplicationTest.class)
@TransactionConfiguration(defaultRollback = true)
@ActiveProfiles(profiles = "embeddedDb")
public class HistoricalProductServiceTest {

    @Autowired
    private HistoricalProductService service;

    @Test
    public void shouldStoreAuditedData(){
        HistoricalProduct historicalProduct = new HistoricalProduct();
        historicalProduct.setName("test");
        historicalProduct.setProductUrl("http");
        historicalProduct.setUnitSize("kg");
        historicalProduct.setUnitSell("123zl/kg");
        historicalProduct.setPrice(new BigDecimal(2));
        historicalProduct.setGrammage(new BigDecimal(12));
        historicalProduct.setGrammagePerUnit("kg");
        historicalProduct.setImgUrl("urlImg");

        service.saveProduct(historicalProduct);

        historicalProduct.setPrice(new BigDecimal(123));
        service.saveProduct(historicalProduct);

        Revisions<Integer, HistoricalProduct> historicData = service.findHistoricDataForProduct(historicalProduct.getProductId());
        Assertions.assertThat(historicData).hasSize(2);
    }
}