CREATE TABLE IF NOT EXISTS PRODUCT (
  product_id          SERIAL      PRIMARY KEY,
  name                VARCHAR(128) NOT NULL,
  internal_shop_id    VARCHAR(32) NOT NULL,
  shop_name           VARCHAR(32) NOT NULL,
  product_url         VARCHAR(256) NOT NULL,
  unit_size           VARCHAR(32) NOT NULL,
  unit_sell           VARCHAR(24)  NOT NULL,
  actual_gross_price  NUMERIC(6,2),
  grammage            NUMERIC(6,2),
  grammage_unit       VARCHAR(32) NOT NULL,
  img_url             VARCHAR(128) NOT NULL
);
