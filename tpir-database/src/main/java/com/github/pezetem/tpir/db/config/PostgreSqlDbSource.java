package com.github.pezetem.tpir.db.config;

import org.hibernate.envers.strategy.ValidityAuditStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.Properties;

@Configuration
@PropertySource("classpath:dbApplication.properties")
@Profile("productionDb")
public class PostgreSqlDbSource implements DbSource{

    @Value("${postgres.url}")
    private String postgresUrl;

    @Value("${postgres.user}")
    private String postgresUser;

    @Value("${postgres.password}")
    private String postgresPassword;

    @Value("${hibernate.show_sql}")
    private String showSql;

    @Value("${hibernate.format_sql}")
    private String formatSql;

    @Override
    public DriverManagerDataSource getDataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        return initDataSource(driverManagerDataSource);
    }

    @Override
    public Properties getJpaProperties() {
        Properties jpaProperties = new Properties();
        jpaProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
        jpaProperties.setProperty("hibernate.jdbc.fetch_size", "1000");
        jpaProperties.setProperty("hibernate.connection.autocommit", "true");
        jpaProperties.setProperty("hibernate.jdbc.batch_size", "0");
        jpaProperties.setProperty("hibernate.show_sql", showSql);
        jpaProperties.setProperty("hibernate.format_sql", formatSql);
        jpaProperties.setProperty("org.hibernate.envers.audit_strategy", ValidityAuditStrategy.class.getName());
        return jpaProperties;
    }

    public DriverManagerDataSource initDataSource(DriverManagerDataSource dataSource) {
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(postgresUrl);
        dataSource.setUsername(postgresUser);
        dataSource.setPassword(postgresPassword);
        return dataSource;
    }
}
