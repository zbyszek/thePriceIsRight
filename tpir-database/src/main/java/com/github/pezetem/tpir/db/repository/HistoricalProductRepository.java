package com.github.pezetem.tpir.db.repository;

import com.github.pezetem.tpir.domain.HistoricalProduct;
import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.history.Revisions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HistoricalProductRepository extends EnversRevisionRepository<HistoricalProduct, Long, Integer>, JpaRepository<HistoricalProduct, Long> {
    Revisions<Integer, HistoricalProduct> findRevisions(Long aLong);

    @Query("select p.productId from HistoricalProduct p where p.shopName = ?1 and p.name = ?2")
    List<Long> findExistingId(String shopName, String name);

    @Query("select count(p) from HistoricalProduct p")
    Long countAll();
}
