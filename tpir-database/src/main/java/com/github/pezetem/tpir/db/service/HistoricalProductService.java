package com.github.pezetem.tpir.db.service;

import com.github.pezetem.tpir.domain.HistoricalProduct;
import com.github.pezetem.tpir.db.repository.HistoricalProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.history.Revisions;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class HistoricalProductService {

    @Autowired
    private HistoricalProductRepository repository;

    public void saveProduct(HistoricalProduct historicalProduct) {
        saveProduct(Arrays.asList(historicalProduct));
    }

    public void saveProduct(List<HistoricalProduct> historicalProducts) {
        List<HistoricalProduct> historicalProductList = historicalProducts.stream().map(product -> {
                    List<Long> storedId = repository.findExistingId(product.getShopName(), product.getName());
                    if (storedId.size() > 0) {
                        product.setProductId(storedId.get(0));
                    }
                    return product;
                }
        ).collect(Collectors.toList());
        repository.save(historicalProductList);
    }

    public Revisions<Integer, HistoricalProduct> findHistoricDataForProduct(Long productId) {
        return repository.findRevisions(productId);
    }

    public Long countAllProducts(){
        return repository.countAll();
    }
}
