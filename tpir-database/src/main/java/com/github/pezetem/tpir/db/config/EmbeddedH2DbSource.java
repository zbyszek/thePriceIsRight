package com.github.pezetem.tpir.db.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.Properties;

@Configuration
@Profile("embeddedDb")
public class EmbeddedH2DbSource implements DbSource{

    @Bean(name = "datasource")
    @Override
    public DriverManagerDataSource getDataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("org.h2.Driver");
        driverManagerDataSource.setUrl("jdbc:h2:mem:foo;DB_CLOSE_DELAY=-1;MODE=PostgreSQL");
        driverManagerDataSource.setUsername("sa");
        driverManagerDataSource.setPassword("");
        return driverManagerDataSource;
    }

    @Override
    public Properties getJpaProperties() {
        Properties jpaProperties = new Properties();
        jpaProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        jpaProperties.setProperty("hibernate.jdbc.fetch_size", "1000");
        jpaProperties.setProperty("hibernate.connection.autocommit", "true");
        jpaProperties.setProperty("hibernate.jdbc.batch_size", "100");
        jpaProperties.setProperty("hibernate.show_sql", "false");
        jpaProperties.setProperty("hibernate.format_sql", "false");
        return jpaProperties;
    }
}
