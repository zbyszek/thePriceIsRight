package com.github.pezetem.tpir.db.config;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.Properties;

public interface DbSource {
    DriverManagerDataSource getDataSource();
    Properties getJpaProperties();
}
