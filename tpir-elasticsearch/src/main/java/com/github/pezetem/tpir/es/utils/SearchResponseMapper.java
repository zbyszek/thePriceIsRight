package com.github.pezetem.tpir.es.utils;

import com.github.pezetem.tpir.domain.Product;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Transactional
public class SearchResponseMapper {
    public List<Product> toProducts(SearchResponse response) {
        List<SearchHit> hitList = Arrays.asList(
                Optional.ofNullable(response)
                        .map(SearchResponse::getHits)
                        .map(SearchHits::hits)
                        .orElse(new SearchHit[0])
        );

        return hitList.stream().map(e -> convertToProduct(e.getSource())).collect(Collectors.toList());
    }

    private Product convertToProduct(Map<String, Object> rawProduct){
        String productUrl = getStringFromRawProduct(rawProduct, "product_url");
        String unitSize = getStringFromRawProduct(rawProduct, "unit_size");
        String shopName = getStringFromRawProduct(rawProduct, "shop_name");
        String unitSell = getStringFromRawProduct(rawProduct, "unit_sell");
        BigDecimal actualGrossPrice = getBigDecimalFromRawProduct(rawProduct);
        String imgUrl = getStringFromRawProduct(rawProduct, "img_url");
        String productName = getStringFromRawProduct(rawProduct, "name");

        return Product.builder()
                .name(productName)
                .imgUrl(imgUrl)
                .price(actualGrossPrice)
                .unitSell(unitSell)
                .unitSize(unitSize)
                .shopName(shopName)
                .imgUrl(imgUrl)
                .productUrl(productUrl)
                .build();
    }

    private BigDecimal getBigDecimalFromRawProduct(Map<String, Object> rawProduct) {
        return new BigDecimal(
                Optional.ofNullable(
                        rawProduct.get("actual_gross_price").toString())
                        .orElse("0"));
    }

    private String getStringFromRawProduct(Map<String, Object> rawProduct, String parameter) {
        return Optional.ofNullable(
                rawProduct.get(parameter).toString())
                .orElse("");
    }
}
