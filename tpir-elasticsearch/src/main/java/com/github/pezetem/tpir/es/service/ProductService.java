package com.github.pezetem.tpir.es.service;

import com.github.pezetem.tpir.domain.Product;
import com.github.pezetem.tpir.es.utils.ElasticQueryBuilder;
import com.github.pezetem.tpir.es.utils.SearchResponseMapper;
import org.elasticsearch.action.search.SearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    private static final String PRODUCT_NAME = "name";
    private SearchResponseMapper responseMapper;
    private ElasticQueryBuilder queryBuilder;

    @Autowired
    public ProductService(SearchResponseMapper responseMapper, ElasticQueryBuilder queryBuilder) {
        this.responseMapper = responseMapper;
        this.queryBuilder = queryBuilder;
    }

    public List<Product> findProductByName(String productName, String shopName) {
        SearchResponse response = queryBuilder.matchQuery(PRODUCT_NAME, productName);
        return responseMapper.toProducts(response);
    }
}