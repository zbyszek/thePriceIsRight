package com.github.pezetem.tpir.es.utils;

import com.github.pezetem.tpir.es.config.ElasticSearchServer;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ElasticQueryBuilder {
    private ElasticSearchServer searchServer;
    private static final String INDEX = "products";

    @Autowired
    public ElasticQueryBuilder(ElasticSearchServer searchServer) {
        this.searchServer = searchServer;
    }

    public SearchResponse matchQuery(String name, String text) {
        QueryBuilder qb = QueryBuilders.matchQuery(name, text);
        return searchServer.getClient()
                .prepareSearch(INDEX)
                .setQuery(qb)
                .execute()
                .actionGet();
    }
}
