package com.github.pezetem.tpir.es.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
@ComponentScan(basePackages = "com.github.pezetem.tpir.es")
@PropertySource("classpath:esApplication.properties")
public class ElasticSearchServer {

    @Value("${elasticsearch.host}")
    private String elasticSearchHost;
    @Value("${elasticsearch.port}")
    private Integer elasticSearchPort;
    @Value("${elasticsearch.cluster_name}")
    private String clusterName;

    @Bean(name = "esClient")
    public Client getClient() {
        Settings settings = getEsSettings();
        TransportClient client = new TransportClient(settings);
        InetAddress esInetAddress = getEsInetAddress();
        TransportAddress esTransportAddress = new InetSocketTransportAddress(esInetAddress, elasticSearchPort);
        client.addTransportAddress(esTransportAddress);
        return client;
    }

    private InetAddress getEsInetAddress() {
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getByName(elasticSearchHost);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return inetAddress;
    }

    private Settings getEsSettings() {
        return ImmutableSettings.settingsBuilder()
                .put("cluster.name", clusterName)
                .put("client.transport.ignore_cluster_name", false)
                .put("node.client", true)
                .put("client.transport.sniff", true)
                .build();
    }
}
