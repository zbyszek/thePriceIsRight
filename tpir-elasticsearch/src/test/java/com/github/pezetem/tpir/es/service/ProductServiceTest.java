package com.github.pezetem.tpir.es.service;

import org.assertj.core.api.Assertions;
import com.github.pezetem.tpir.es.EsApplicationTest;
import com.github.pezetem.tpir.domain.Product;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EsApplicationTest.class)
@TransactionConfiguration(defaultRollback = true)
public class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @Ignore("fix test for travis-ci")
    @Test
    public void shouldReturnAtLeastOneProduct() {
        String productName = "Mleko";
        String shopName = "Alma";
        List<Product> products = productService.findProductByName(productName, shopName);
        Assertions.assertThat(products).hasAtLeastOneElementOfType(Product.class);
    }
}