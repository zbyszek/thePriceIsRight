package com.github.pezetem.tpir.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsApplicationTest {
    public static void main(String[] args){
        SpringApplication.run(EsApplicationTest.class);
    }
}
