package com.github.pezetem.tpir.es.utils;

import org.assertj.core.api.Assertions;
import com.github.pezetem.tpir.domain.Product;
import org.elasticsearch.action.search.SearchResponse;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

public class SearchResponseMapperTest {
    private SearchResponseMapper responseMapper = new SearchResponseMapper();

    @Test
    public void shouldHandleNoHitsFromES() {
        SearchResponse emptyResponse = Mockito.mock(SearchResponse.class);
        Mockito.when(emptyResponse.getHits()).thenReturn(null);

        List<Product> products = responseMapper.toProducts(emptyResponse);
        Assertions.assertThat(products).isEmpty();
    }


}